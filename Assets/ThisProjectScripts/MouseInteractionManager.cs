﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MouseInteractionManager : MonoBehaviour
{
    public GameObject PauseMenu; //set in OnSceneLoaded

    private GameController GameController; //set in OnSceneLoaded
    private GameObject FadeOut; //set in OnSceneLoaded
    private DeckManager DeckManager; //set in OnSceneLoaded
    private TMP_Text WarningText; //set in OnSceneLoaded
    private GameObject CardHeld; //set in Update
    private TMP_Text PlayerNumberText; //set in OnSceneLoaded
    private bool HoldingCard = false;
    [SerializeField]
    private AudioSource TakeCard; //set in Inspector
    [SerializeField]
    private AudioSource UISound; //set in Inspector

    private List<Tuple<GameObject, Vector3>> clickedGOs; //needed in PointerInteractionSolution

    public void OnNewScene() //chiamato quando cambia la scena
    {
        if (GameController.GamePaused)
        {
            PauseMenu.SetActive(false);
            GameController.UnPause();
        }

        FadeOut.SetActive(true);
    }

    public void OnSceneLoaded() //chiamato quando la nuova scena viene caricata
    {
        if (GameController == null)
            GameController = gameObject.GetComponent<GameController>();

        if (GameController.MainMenu == false) //se la nuova scena è una scena di gioco
        {
            DeckManager = GameObject.Find("DeckZone").GetComponent<DeckManager>();
            WarningText = GameObject.Find("WarningMessage").GetComponent<TMP_Text>();
            PauseMenu = GameObject.Find("PauseMenu");
            FadeOut = GameObject.Find("FadeOut");
            PauseMenu.SetActive(false);
            FadeOut.SetActive(false);
        }
        else //se la nuova scena è il main menu
        {
            PlayerNumberText = GameObject.Find("PlayerNumberText").GetComponent<TMP_Text>();
            PlayerNumberText.text = "" + GameController.PlayerNumber;
            FadeOut = GameObject.Find("FadeOut");
            FadeOut.SetActive(false);
        }
    }

    private void Update()
    {
        // Input.GetMouseButton(0) = Tasto sinistro tenuto premuto
        // Input.GetMouseButtonDown(0) = In quel frame è stato premuto il tasto sinistro

        if (GameController.MainMenu) //Gestione main menu
        {
            if (Input.GetMouseButtonDown(0))
            {
                clickedGOs = PointerInteractionSolution("UI", Input.mousePosition);
                if (clickedGOs.Count > 0)
                {
                    foreach (var go in clickedGOs)
                    {
                        if (go.Item1.name.Contains("NewGame")) //Cliccato il pulsante per avviare un nuovo gioco
                        {
                            GameController.OnNewGame();
                            UISound.Play();
                            break;
                        }
                        if (go.Item1.name.Contains("MuteAudio")) //Cliccato il pulsante per mutare l'audio
                        {
                            if (AudioListener.volume == 1f)
                                AudioListener.volume = 0f;
                            else
                                AudioListener.volume = 1f;
                            UISound.Play();
                            break;
                        }
                        if (go.Item1.name.Contains("ChangePlayerNumber")) //Cliccato il pulsante per variare il numero di giocatori
                        {
                            if (GameController.PlayerNumber == 3)
                            {
                                GameController.PlayerNumber = 1;
                                PlayerNumberText.text = "" + 1;
                            }
                            else
                            {
                                GameController.PlayerNumber++;
                                PlayerNumberText.text = "" + GameController.PlayerNumber;
                            }
                            UISound.Play();
                            break;
                        }
                        if (go.Item1.name.Contains("ChangePlayerSkin")) //Cliccato uno dei pulsanti per cambiare la skin dei giocatori
                        {
                            if (go.Item1.name.Contains("0"))
                            {
                                GameController.NextSkin(0);
                            }
                            else if (go.Item1.name.Contains("1"))
                            {
                                GameController.NextSkin(1);
                            }
                            else if (go.Item1.name.Contains("2"))
                            {
                                GameController.NextSkin(2);
                            }
                            UISound.Play();
                            break;
                        }
                    }
                }
            }
        }
        else if (GameController.GamePaused) //Gestione menu di pausa
        {
            if (Input.GetMouseButtonDown(0))
            {
                clickedGOs = PointerInteractionSolution("UI", Input.mousePosition);
                if (clickedGOs.Count > 0)
                {
                    foreach (var go in clickedGOs)
                    {
                        if (go.Item1.name.Contains("Continue")) //Cliccato il pulsante per tornare al gioco
                        {
                            GameController.UnPause();
                            PauseMenu.SetActive(false);
                            UISound.Play();
                            break;
                        }

                        if (go.Item1.name.Contains("New Game")) //Cliccato il pulsante per creare un nuovo gioco
                        {
                            GameController.OnNewGame();
                            UISound.Play();
                            break;
                        }

                        if (go.Item1.name.Contains("Main Menu")) //Cliccato il pulsante per tornare al menu principale
                        {
                            GameController.OnReturnMainMenu();
                            UISound.Play();
                            break;
                        }
                    }
                }
            }
        }
        else //Gestione gioco
        {
            //----------------------- Gestione pulsante premuto istantaneamente
            if (Input.GetMouseButtonDown(0))
            {
                clickedGOs = PointerInteractionSolution("UI+GAME", Input.mousePosition);
                if (clickedGOs.Count > 0)
                {
                    foreach (var go in clickedGOs)
                    {
                        if (go.Item1.name.Contains("Deck")) //Toccato il mazzo
                        {
                            if (DeckManager.AlreadyShuffled == false)
                                WarningText.text = "You should shuffle the deck first!"; //Avvisa il giocatore di mischiare il mazzo se non è già mischiato
                            CardHeld = DeckManager.TakeCard();
                            TakeCard.Play();
                            CardHeld.GetComponent<Rigidbody>().useGravity = false;
                            HoldingCard = true;
                            CardHeld.GetComponent<CardManager>().Held = true;
                            break;
                        }

                        if (go.Item1.name.Contains("Shuffle")) //Cliccato il pulsante per mischiare il mazzo
                        {
                            DeckManager.RequestedShuffle();
                            break;
                        }

                        if (go.Item1.name.Equals("PauseButton")) //Cliccato il pulsante per mettere in pausa il gioco
                        {
                            GameController.Pause();
                            PauseMenu.SetActive(true);
                            UISound.Play();
                            break;
                        }

                        if (go.Item1.name.Equals("StopButton")) //Cliccato il pulsante per terminare l'aggiunta di carte per il dealer
                        {
                            GameController.DealerZone.Okay();
                            UISound.Play();
                            break;
                        }
                    }
                }
            }


            //----------------------- Gestione pulsante tenuto premuto
            if (Input.GetMouseButton(0) && GameController.GamePaused == false && GameController.MainMenu == false)
            {
                if (HoldingCard) //Gestione del movimento e del lancio della carta con il mouse
                {
                    float movedownY = 0.0f;
                    float sensitivityY = 0.028f;
                    movedownY += Input.GetAxis("Mouse Y") * sensitivityY;
                    if (Input.GetAxis("Mouse Y") != 0)
                    {
                        //m_cardHeld.transform.Translate(Vector3.forward * movedownY, Space.World);
                        CardHeld.GetComponent<Rigidbody>().AddForce(Vector3.forward * movedownY * 100);
                    }
                    movedownY = 0.0f;

                    float movedownX = 0.0f;
                    float sensitivityX = 0.028f;
                    movedownX += Input.GetAxis("Mouse X") * sensitivityX;
                    if (Input.GetAxis("Mouse X") != 0)
                    {
                        //m_cardHeld.transform.Translate(Vector3.right * movedownX, Space.World);
                        CardHeld.GetComponent<Rigidbody>().AddForce(Vector3.right * movedownX * 100);
                    }
                    movedownX = 0.0f;

                }
            }

            //----------------------- Gestione pulsante sollevato
            if (Input.GetMouseButtonUp(0))
            {
                LeaveCard();
            }

            //----------------------- Gestione pulsante non premuto
            if (!Input.GetMouseButton(0) && !Input.GetMouseButtonDown(0) && !Input.GetMouseButtonUp(0))
            {
                //Do nothing
            }
        }
    }

    public void LeaveCard()
    {
        if (HoldingCard)
        {
            HoldingCard = false;
            CardHeld.GetComponent<CardManager>().Held = false;
            CardHeld.GetComponent<Rigidbody>().useGravity = true;
            CardHeld = null;
        }
    }

    //INPUT: mode è una string che può essere UI+GAME, UI, oppure GAME, a seconda della scelta individua i GameObject specifici della UI gestita da Unity con gli eventi, specifici della scena di gioco o entrambi
    //       mousePosition è la posizione del mouse sullo schermo ottenuta con Input.mousePosition
    //OUTPUT: una lista di tuple (Gameobject, Vector3) che indica tutti i game object trovati nella posizione del mouse e la posizione esatta del punto in cui il gameobject è stato toccato nel mondo.
    //NOTA - codice ereditato da progetti precedenti
    private static List<Tuple<GameObject, Vector3>> PointerInteractionSolution(string mode, Vector3 mousePosition)
    {
        List<Tuple<GameObject, Vector3>> gameObjects = new List<Tuple<GameObject, Vector3>>();
        if (mode.Equals("UI+GAME"))
        {
            //GAME
            var ray = Camera.main.ScreenPointToRay(mousePosition);
            Camera.main.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, Camera.main.farClipPlane));
            RaycastHit[] hitInfo;
            RaycastHit2D[] hitInfo2D;
            if (Physics.Raycast(ray) || Physics2D.Raycast(ray.origin, ray.direction))
            {
                hitInfo = Physics.RaycastAll(ray);
                //Dentro hitInfo stanno tutti gli oggetti colpiti
                foreach (var go in hitInfo)
                {
                    gameObjects.Add(new Tuple<GameObject, Vector3>(go.transform.gameObject, go.point));
                }
                hitInfo2D = Physics2D.RaycastAll(ray.origin, ray.direction);
                foreach (var go in hitInfo2D)
                {
                    gameObjects.Add(new Tuple<GameObject, Vector3>(go.transform.gameObject, go.point));
                }
            }

            //UI
            if (EventSystem.current != null)
            {
                PointerEventData pointer = new PointerEventData(EventSystem.current);
                pointer.position = mousePosition;

                List<RaycastResult> raycastResults = new List<RaycastResult>();
                EventSystem.current.RaycastAll(pointer, raycastResults);

                if (raycastResults.Count > 0)
                {
                    foreach (var go in raycastResults)
                    {
                        gameObjects.Add(new Tuple<GameObject, Vector3>(go.gameObject, go.worldPosition));
                    }
                }
            }
            return gameObjects;
        }
        else if (mode.Equals("UI"))
        {
            //UI
            if (EventSystem.current != null)
            {
                PointerEventData pointer = new PointerEventData(EventSystem.current);
                pointer.position = mousePosition;

                List<RaycastResult> raycastResults = new List<RaycastResult>();
                EventSystem.current.RaycastAll(pointer, raycastResults);

                if (raycastResults.Count > 0)
                {
                    foreach (var go in raycastResults)
                    {
                        gameObjects.Add(new Tuple<GameObject, Vector3>(go.gameObject, go.worldPosition));
                    }
                }
            }
            return gameObjects;
        }
        else if (mode.Equals("GAME"))
        {
            //GAME
            var ray = Camera.main.ScreenPointToRay(mousePosition);
            Camera.main.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, Camera.main.farClipPlane));
            RaycastHit[] hitInfo;
            RaycastHit2D[] hitInfo2D;
            if (Physics.Raycast(ray) || Physics2D.Raycast(ray.origin, ray.direction))
            {
                hitInfo = Physics.RaycastAll(ray);
                //Dentro hitInfo stanno tutti gli oggetti colpiti
                foreach (var go in hitInfo)
                {
                    gameObjects.Add(new Tuple<GameObject, Vector3>(go.transform.gameObject, go.point));
                }
                hitInfo2D = Physics2D.RaycastAll(ray.origin, ray.direction);
                foreach (var go in hitInfo2D)
                {
                    gameObjects.Add(new Tuple<GameObject, Vector3>(go.transform.gameObject, go.point));
                }
            }
            return gameObjects;
        }
        else
            return null;
    }
}
