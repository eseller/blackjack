﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using UnityEditor;
using System.IO;

public class GameController : MonoBehaviour
{
    public bool GamePaused = false;
    public bool MainMenu = true; //set in OnLevelFinishedLoading
    public int[] PLAYER_PERCENTAGE = { 70, 80, 90 };
    public DealerZone DealerZone; //set in OnLevelFinishedLoading
    public GameObject StopButton; //set in OnLevelFinishedLoading
    public int PlayerNumber = 3;

    private MouseInteractionManager MouseInteractionManager; //set in OnLevelFinishedLoading
    private List<GameObject> ToPause = new List<GameObject>(); //set in Pause
    private List<GameObject> Players = new List<GameObject>(); //set in SetupPlayers
    private List<GameObject> PlayerPanels = new List<GameObject>(); //set in SetupPlayers
    private List<PlayerZone> PlayerZones = new List<PlayerZone>(); //set in SetupPlayers
    private GameObject DealerZoneGO; //set in OnLevelFinishedLoading
    private bool DealerPhase = false;
    private bool GameEnded = false;
    private int PlayersDone = 0; //set in OnLevelFinishedLoading
    private Image P0Skin; //set in SetupSkinsMainMenu
    private Image P1Skin; //set in SetupSkinsMainMenu
    private Image P2Skin; //set in SetupSkinsMainMenu
    private int P0SkinIndex = 5; //set in NextSkin
    private int P1SkinIndex = 5; //set in NextSkin
    private int P2SkinIndex = 5; //set in NextSkin
    [SerializeField]
    private GameObject[] Skins = new GameObject[41]; //set in Inspector
    [SerializeField]
    private Sprite[] Sprites = new Sprite[41]; //set in Awake (only the first time), set in Inspector

    [SerializeField]
    private AudioSource VictorySound; //set in inspector
    [SerializeField]
    private AudioSource DefeatSound; //set in inspector

    private void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("GameController");
        if (objs.Length > 1) //si assicura che GameController sia unico
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject); //persistenza tra le scene

        int i = 0;
        //foreach (GameObject g in Skins) //Solo una volta fuori dalla build per costruire gli sprites
        //{
        //    Sprites[i] = Sprite.Create(AssetPreview.GetAssetPreview(g), new Rect(0, 0, 128, 128), new Vector2());
        //    System.IO.File.WriteAllBytes(Application.dataPath + "/" + g.name + ".png", Sprites[i].texture.EncodeToPNG());
        //    i++;
        //}
    }

    public void OnNewGame() //chiamato quando viene richiesto di avviare una nuova partita
    {
        MouseInteractionManager.OnNewScene();
        Players.Clear();
        PlayerPanels.Clear();
        PlayerZones.Clear();
        StartCoroutine("ChangingSceneNewGame");
    }

    public void OnReturnMainMenu() //chiamato quando viene richiesto di tornare al menu principale
    {
        MouseInteractionManager.OnNewScene();
        Players.Clear();
        PlayerPanels.Clear();
        PlayerZones.Clear();
        StartCoroutine("ChangingSceneMainMenu");
    }

    private IEnumerator ChangingSceneMainMenu()
    {
        yield return new WaitForSeconds(0.9f);
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    private IEnumerator ChangingSceneNewGame()
    {
        yield return new WaitForSeconds(0.9f);
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void Pause() //chiamato per mettere in pausa il gioco
    {
        ToPause.Clear();

        ToPause.AddRange(GameObject.FindGameObjectsWithTag("ToPause"));
        foreach (GameObject g in ToPause)
        {
            g.SetActive(false);
        }
        GamePaused = true;
    }

    public void UnPause() //chiamato per togliere il gioco dalla pausa
    {
        foreach (GameObject g in ToPause)
        {
            if (g != null)
                g.SetActive(true);
        }
        GamePaused = false;
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode) //chiamato quando la scena richiesta è stata caricata
    {
        if (scene.buildIndex == 1) //Scena di gioco
        {
            if (MouseInteractionManager == null)
            {
                MouseInteractionManager = gameObject.GetComponent<MouseInteractionManager>();
            }
            MainMenu = false;
            MouseInteractionManager.OnSceneLoaded();
            DealerPhase = false;
            GameEnded = false;
            GamePaused = false;
            PlayersDone = 0;
            SetupPlayers();
            SetupSkinsGame();
            DealerZoneGO = GameObject.Find("DealerPlay");
            DealerZone = DealerZoneGO.GetComponent<DealerZone>();
            StopButton = GameObject.Find("StopButton");
            DealerZoneGO.SetActive(false);
            StopButton.SetActive(false);
        }
        else if (scene.buildIndex == 0) //Scena del menu principale
        {
            if (MouseInteractionManager == null)
            {
                MouseInteractionManager = gameObject.GetComponent<MouseInteractionManager>();
            }
            MainMenu = true;
            GamePaused = false;
            SetupSkinsMainMenu();
            MouseInteractionManager.OnSceneLoaded();
        }
    }

    private void SetupPlayers() //chiamato in OnLevelFinishedLoading nel caso la scena sia quella di gioco per creare i riferimenti ai giocatori e settarne il numero
    {
        Players.Add(GameObject.Find("Player0Spawn"));
        PlayerPanels.Add(GameObject.Find("P0Panel"));
        Players.Add(GameObject.Find("Player1Spawn"));
        PlayerPanels.Add(GameObject.Find("P1Panel"));
        Players.Add(GameObject.Find("Player2Spawn"));
        PlayerPanels.Add(GameObject.Find("P2Panel"));

        PlayerZones.Add(Players[0].GetComponent<PlayerZone>());
        PlayerZones.Add(Players[1].GetComponent<PlayerZone>());
        PlayerZones.Add(Players[2].GetComponent<PlayerZone>());


        if (PlayerNumber == 2)
        {
            Players[2].SetActive(false);
            PlayerPanels[2].SetActive(false);

            Players.RemoveAt(2);
            PlayerPanels.RemoveAt(2);
            PlayerZones.RemoveAt(2);

        }
        else if (PlayerNumber == 1)
        {
            Players[1].SetActive(false);
            PlayerPanels[1].SetActive(false);
            Players[2].SetActive(false);
            PlayerPanels[2].SetActive(false);


            Players.RemoveAt(2);
            PlayerPanels.RemoveAt(2);
            PlayerZones.RemoveAt(2);
            Players.RemoveAt(1);
            PlayerPanels.RemoveAt(1);
            PlayerZones.RemoveAt(1);
        }
    }

    private void LateUpdate() //determina lo stato del gioco attuale ad ogni frame
    {
        if (!MainMenu) //se si è nella scena di gioco
        {
            if (!DealerPhase) //se si è nella fase dei giocatori
            {
                PlayersDone = 0;
                foreach (PlayerZone pz in PlayerZones) //determina quanti player sono ok o hanno sballato
                {
                    if (pz.okay || pz.bust)
                    {
                        PlayersDone++;
                    }
                }
                if (PlayersDone >= PlayerZones.Count) //tutti i player sono ok o hanno sballato, passo alla fase del dealer
                {
                    DealerPhase = true;
                    DealerZoneGO.SetActive(true);
                }
            }
            else //se si è nella fase del dealer
            {
                EndGame();
            }
        }
    }

    private void EndGame() //determina se il dealer ha concluso la sua fase e il gioco si è concluso, assegnando la vittoria
    {
        if (!GameEnded) //si assicura che la sequenza finale di conclusione del gioco si inneschi una sola volta
        {
            if (DealerZone.okay) //il dealer è ok, la vittoria viene assegnata
            {
                GameEnded = true;
                List<int> Scores = new List<int>();
                foreach (PlayerZone pz in PlayerZones) //valuta il punteggio dei players
                {
                    if (pz.bust == false)
                        Scores.Add(pz.TotalValue);
                }
                Scores.Add(DealerZone.TotalValue);
                Scores.Sort();
                foreach (PlayerZone pz in PlayerZones) //gestisce le animazioni dei players a seconda del loro risultato
                {
                    if (Scores[Scores.Count - 1] == pz.TotalValue && pz.bust == false && pz.TotalValue > DealerZone.TotalValue)
                    {
                        pz.Victory();
                    }
                    else
                    {
                        pz.Bust();
                    }
                }
                if (Scores[Scores.Count - 1] == DealerZone.TotalValue) //gestisce il messaggio al dealer e il suono di vittoria/sconfitta a seconda del suo risultato
                {
                    DealerZone.Victory();
                    VictorySound.Play();
                    StartCoroutine("PauseEndGame");
                }
                else
                {
                    DealerZone.Lost();
                    DefeatSound.Play();
                    StartCoroutine("PauseEndGame");
                }
            }
            else if (DealerZone.bust) //il dealer ha sballato, la vittoria viene assegnata a tutti i players
            {
                GameEnded = true;
                foreach (PlayerZone pz in PlayerZones)
                {
                    pz.Victory();
                }
                DefeatSound.Play();
                StartCoroutine("PauseEndGame");
            }
        }
    }

    private void SetupSkinsMainMenu() //chiamato quando viene caricato il menu principale, si assicura che le anteprime delle skin siano corrette
    {
        P0Skin = GameObject.Find("Preview0").GetComponent<Image>();
        P0Skin.sprite = Sprites[P0SkinIndex];
        P1Skin = GameObject.Find("Preview1").GetComponent<Image>();
        P1Skin.sprite = Sprites[P1SkinIndex];
        P2Skin = GameObject.Find("Preview2").GetComponent<Image>();
        P2Skin.sprite = Sprites[P2SkinIndex];
    }

    private void SetupSkinsGame() //chiamato quando viene caricato il gioco, si assicura di modificare i players utilizzando le skin scelte nel menu principale
    {
        GameObject old = Players[0].transform.Find("man-casual_Rig 1").gameObject;
        Destroy(old);
        GameObject neew = Instantiate(Skins[P0SkinIndex], Players[0].transform);
        PlayerZones[0].PlayerAnimator = neew.GetComponent<Animator>();

        if (PlayerNumber > 1)
        {
            GameObject old1 = Players[1].transform.Find("man-casual_Rig 1").gameObject;
            Destroy(old1);
            GameObject neew1 = Instantiate(Skins[P1SkinIndex], Players[1].transform);
            PlayerZones[1].PlayerAnimator = neew1.GetComponent<Animator>();
        }

        if (PlayerNumber > 2)
        {
            GameObject old2 = Players[2].transform.Find("man-casual_Rig 1").gameObject;
            Destroy(old2);
            GameObject neew2 = Instantiate(Skins[P2SkinIndex], Players[2].transform);
            PlayerZones[2].PlayerAnimator = neew2.GetComponent<Animator>();
        }

    }

    public void NextSkin(int playerindex) //aggiorna la scelta delle skin quando viene premuto il pulsante apposito nel menu principale
    {
        if (playerindex == 0)
        {
            if (P0SkinIndex <= 39)
                P0SkinIndex++;
            else
                P0SkinIndex = 0;
            P0Skin.sprite = Sprites[P0SkinIndex];
        }
        else if (playerindex == 1)
        {
            if (P1SkinIndex <= 39)
                P1SkinIndex++;
            else
                P1SkinIndex = 0;
            P1Skin.sprite = Sprites[P1SkinIndex];
        }
        else if (playerindex == 2)
        {
            if (P2SkinIndex <= 39)
                P2SkinIndex++;
            else
                P2SkinIndex = 0;
            P2Skin.sprite = Sprites[P2SkinIndex];
        }
    }

    private IEnumerator PauseEndGame() //pausa il gioco quando termina
    {
        yield return new WaitForSeconds(4f);
        if (!GamePaused)
        {
            Pause();
            MouseInteractionManager.PauseMenu.SetActive(true);
        }
    }
}
