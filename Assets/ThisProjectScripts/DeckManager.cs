﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DeckManager : MonoBehaviour
{
    public bool AlreadyShuffled = false; //set in MouseInteractionManager

    [SerializeField]
    private Transform CardSpawnTransform; //set in inspector
    [SerializeField]
    private TMP_Text ShuffleText; //set in inspector
    [SerializeField]
    private TMP_Text WarningText; //set in inspector
    [SerializeField]
    private MeshRenderer Deck; //set in inspector
    [SerializeField]
    private AudioSource ShuffleSound; //set in inspector
    private static GameObject[] FullDeckArray = new GameObject[52]; //needed for list
    [SerializeField]
    private List<GameObject> FullDeck = new List<GameObject>(FullDeckArray); //set in Inspector
    private List<GameObject> CurrentDeck; //set in start

    // Start is called before the first frame update
    void Start()
    {
        int i = 0;
        foreach (GameObject g in FullDeck)
        {
            FullDeckArray[i] = g;
            i++;
        }
        CurrentDeck = new List<GameObject>(FullDeckArray);

    }

    public void RequestedShuffle()
    {
        if (!AlreadyShuffled)
        {
            Shuffle(CurrentDeck);
            ShuffleSound.Play();
            ShuffleText.color = Color.gray;
            WarningText.text = "Shuffled!"; //Avvisa il player che il mazzo è stato mischiato
            AlreadyShuffled = true;
        }
        else
        {
            WarningText.text = "Cannot shuffle again"; //Avvisa il player che il mazzo non può essere mischiato di nuovo
            return;
        }

    }

    private static List<GameObject> Shuffle(List<GameObject> aList) //Fisher_Yates_CardDeck_Shuffle
    {

        System.Random _random = new System.Random();

        GameObject myGO;

        int n = aList.Count;
        for (int i = 0; i < n; i++)
        {
            // NextDouble returns a random number between 0 and 1.
            // ... It is equivalent to Math.random() in Java.
            int r = i + (int)(_random.NextDouble() * (n - i));
            myGO = aList[r];
            aList[r] = aList[i];
            aList[i] = myGO;
        }

        return aList;
    }

    public GameObject TakeCard() //instanzia la prossima carta nel mazzo e la ritorna
    {
        if (CurrentDeck.Count > 1)
        {
            GameObject card = CurrentDeck[0];
            CurrentDeck.RemoveAt(0);
            return GameObject.Instantiate(card, CardSpawnTransform.position, CardSpawnTransform.rotation);
        }
        else
        {
            WarningText.text = "Deck ended!"; //avvisa il player che il mazzo è finito
            GetComponent<BoxCollider>().enabled = false; //disattiva il collider
            Deck.enabled = false; //fa sparire il mazzo

            GameObject card = CurrentDeck[0];
            CurrentDeck.RemoveAt(0);
            return GameObject.Instantiate(card, CardSpawnTransform.position, CardSpawnTransform.rotation);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Cards")) //rimette la carta nel mazzo (quando viene lasciata sopra al mazzo)
        {
            Debug.Log("Collision Deck");
            ResetCard(collision.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other) //rimette la carta nel mazzo (quando sta ritornando al mazzo perchè non associata ad alcuna zona carta)
    {
        if (other.gameObject.tag.Equals("Cards")) 
        {
            ResetCard(other.gameObject);
        }
    }

    private void ResetCard(GameObject card) //ripristina la carta nel mazzo
    {
        string cardname = RecognizeCard(card.name);
        CurrentDeck.Add(FullDeck.Find((x) => x.name.Contains(cardname)));
        GameObject.Destroy(card);
    }

    private string RecognizeCard(string name)
    {
        if (name.Contains("Heart"))
        {
            return name.Substring(name.LastIndexOf("H"), 7);
        }
        else if (name.Contains("Diamond"))
        {
            return name.Substring(name.LastIndexOf("D"), 9);
        }
        else if (name.Contains("Club"))
        {
            return name.Substring(name.LastIndexOf("b"), 3);
        }
        else if (name.Contains("Spade"))
        {
            return name.Substring(name.LastIndexOf("S"), 7);
        }
        else return null;
    }
}
