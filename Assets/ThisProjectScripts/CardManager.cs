﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CardManager : MonoBehaviour
{

    public bool Held = true; //set by MouseInteractionManager

    private int Value = 0; //set in awake
    private bool Still = false;
    private bool Returning = false;
    private bool IsTrigger = false;
    private bool InAZone = false;
    private Transform DeckZone; //set in start
    private Rigidbody Rigidbody; //set in awake
    private PlayerZone PlayerZone; //set in on collision enter
    private DealerZone DealerZone; //set in on collision enter

    private void Awake()
    {
        Value = CalculateValue(gameObject.name);
        Rigidbody = GetComponent<Rigidbody>();
    }

    void Start()
    {
        DeckZone = GameObject.Find("DeckZone").transform;
    }

    private IEnumerator CheckifStill()
    {
        yield return new WaitForSeconds(1f);
        if (Rigidbody != null && Rigidbody.velocity.x < 0.01f && Rigidbody.velocity.y < 0.01f && Rigidbody.velocity.z < 0.01f)
        { Returning = true; }
        else
            Still = false; 
    }

    private void OnEnable()
    {
        if (Still && !Returning)
        StartCoroutine("CheckifStill");
    }

    void FixedUpdate() //In FixedUpdate gestisco la fase di ritorno della carta nel mazzo se non è associata a nessun giocatore
    {
        if (!InAZone)
        {
            if (!Still)
            {
                if (Rigidbody.velocity.x < 0.01f && Rigidbody.velocity.y < 0.01f && Rigidbody.velocity.z < 0.01f && Held == false)
                {
                    Still = true;
                    StartCoroutine("CheckifStill");
                }
            }
            if (Returning)
            {
                if (!IsTrigger)
                {
                    Rigidbody.useGravity = false;
                    GetComponent<BoxCollider>().isTrigger = true;
                    IsTrigger = true;
                }
                transform.position = Vector3.MoveTowards(transform.position, DeckZone.position, Time.fixedDeltaTime);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Contains("Player")) //Cerca di aggiungere la carta al player
        {
            PlayerZone = collision.gameObject.GetComponentInParent<PlayerZone>();
            if (PlayerZone.AddCard(gameObject))
                InAZone = true;
        }
        if (collision.gameObject.tag.Contains("Dealer")) //Cerca di aggiungere la carta al dealer
        {
            DealerZone = collision.gameObject.GetComponentInParent<DealerZone>();
            if (DealerZone.AddCard(gameObject))
                InAZone = true;
        }
    }

    public static int CalculateValue(string name) //Calcola il valore della carta basandosi sul suo nome
    {
        if (name.Contains("Heart"))
        {
            return Int32.Parse(name.Substring(name.LastIndexOf("H")+5, 2));
        }
        else if (name.Contains("Diamond"))
        {
            return Int32.Parse(name.Substring(name.LastIndexOf("D")+7, 2));
        }
        else if (name.Contains("Club"))
        {
            return Int32.Parse(name.Substring(name.LastIndexOf("b")+1, 2));
        }
        else if (name.Contains("Spade"))
        {
            return Int32.Parse(name.Substring(name.LastIndexOf("S")+5, 2));
        }
        else return 0;
    }
}
