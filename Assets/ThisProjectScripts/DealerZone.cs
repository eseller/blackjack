﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DealerZone : MonoBehaviour
{

    public bool okay = false;
    public bool bust = false;
    public int TotalValue = 0;

    private MouseInteractionManager mouseInteractionManager; //set in start
    private List<GameObject> CardsInZone = new List<GameObject>(); //set in addcard
    private List<Transform> Zones = new List<Transform>(); //set in awake
    [SerializeField]
    private Transform Base1; //set in inspector
    [SerializeField]
    private Transform Base2; //set in inspector
    [SerializeField]
    private Transform Card1; //set in inspector
    [SerializeField]
    private Transform Card2; //set in inspector
    [SerializeField]
    private Transform Card3; //set in inspector
    [SerializeField]
    private Transform Card4; //set in inspector
    [SerializeField]
    private Transform Card5; //set in inspector
    [SerializeField]
    private Transform Card6; //set in inspector
    [SerializeField]
    private Transform Card7; //set in inspector
    [SerializeField]
    private Transform Card8; //set in inspector
    [SerializeField]
    private Transform Card9; //set in inspector
    [SerializeField]
    private TMP_Text WarningText; //set in inspector
    [SerializeField]
    private TMP_Text DealerSummary; //set in inspector
    [SerializeField]
    private GameObject StopButton; //set in inspector

    private void Awake()
    {
        Zones.Add(Base1);
        Zones.Add(Base2);
        Zones.Add(Card1);
        Zones.Add(Card2);
        Zones.Add(Card3);
        Zones.Add(Card4);
        Zones.Add(Card5);
        Zones.Add(Card6);
        Zones.Add(Card7);
        Zones.Add(Card8);
        Zones.Add(Card9);
    }

    private void Start()
    {
        mouseInteractionManager = GameObject.Find("GameController").GetComponent<MouseInteractionManager>();
    }

    public bool AddCard(GameObject card)
    {
        if (okay) //Se il dealer è ok non permette l'aggiunta della carta
        {
            StopButton.SetActive(false);
            return false;
        }

        if (CardsInZone.Count < 11) //Ci possono essere massimo 11 carte nella zona delle carte
        {
            if (CardsInZone.Count < 2) //Se non sono ancora state date le due carte iniziali, aggiunge la carta
            {
                int newcardposition = CardsInZone.Count;
                Destroy(card.GetComponent<Rigidbody>());
                Destroy(card.GetComponent<Collider>());
                mouseInteractionManager.LeaveCard();
                card.GetComponent<Transform>().position = Zones[newcardposition].position;
                card.GetComponent<Transform>().rotation = Zones[newcardposition].rotation;
                CardsInZone.Add(card);
                TotalValue = TotalValue + CardManager.CalculateValue(card.name);
                DealerSummary.text = "You: " + TotalValue;
                if (CardsInZone.Count == 2) //Se questa era la seconda carta data inizia la fase di decisione
                {
                    if (TotalValue < 22)
                        Decide();
                    else
                        Bust();
                }
                return true;
            }
            if (TotalValue < 22) //Se le carte iniziali sono state date, e se non ha sballato, aggiunge la carta e valuta il risultato
            {
                int newcardposition = CardsInZone.Count;
                Destroy(card.GetComponent<Rigidbody>());
                Destroy(card.GetComponent<Collider>());
                mouseInteractionManager.LeaveCard();
                card.GetComponent<Transform>().position = Zones[newcardposition].position;
                card.GetComponent<Transform>().rotation = Zones[newcardposition].rotation;
                CardsInZone.Add(card);
                TotalValue = TotalValue + CardManager.CalculateValue(card.name);
                DealerSummary.text = "You: " + TotalValue;
                if (TotalValue >= 22)
                {
                    Bust();
                }
                else
                {
                    Decide();
                }
                return true;
            }
            else //Ha sballato, non si possono aggiungere altre carte
            {
                StopButton.SetActive(false);
                return false;
            }

        }
        else //Ha 11 carte, non si possono aggiungere altre carte
        {
            WarningText.text = ("Too many cards for dealer");
            StopButton.SetActive(false);
            return false;
        }
    }

    private void Decide()
    {
        StopButton.SetActive(true);
    }

    public void Okay()
    {
        StopButton.SetActive(false);
        okay = true;
    }

    private void Bust()
    {
        StopButton.SetActive(false);
        WarningText.text = ("Bust! You lose!");
        bust = true;
    }

    public void Lost()
    {
        StopButton.SetActive(false);
        WarningText.text = ("You lose!");
    }

    public void Victory()
    {
        StopButton.SetActive(false);
        WarningText.text = ("You win!");
    }
}
