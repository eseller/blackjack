﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerZone : MonoBehaviour
{
    private MouseInteractionManager mouseInteractionManager; //set in start
    public bool okay = false;
    public bool bust = false;
    public int TotalValue = 0;
    public Animator PlayerAnimator; //set in SetupSkinsGame

    private GameController GameController; //set in start
    private List<GameObject> CardsInZone = new List<GameObject>(); //set in AddCard
    private List<Transform> Zones = new List<Transform>(); //set in awake
    private TMP_Text PlayerSummary; //set in start
    [SerializeField]
    private Transform Base1; //set in inspector
    [SerializeField]
    private Transform Base2; //set in inspector
    [SerializeField]
    private Transform Card1; //set in inspector
    [SerializeField]
    private Transform Card2; //set in inspector
    [SerializeField]
    private Transform Card3; //set in inspector
    [SerializeField]
    private Transform Card4; //set in inspector
    [SerializeField]
    private Transform Card5; //set in inspector
    [SerializeField]
    private Transform Card6; //set in inspector
    [SerializeField]
    private Transform Card7; //set in inspector
    [SerializeField]
    private Transform Card8; //set in inspector
    [SerializeField]
    private Transform Card9; //set in inspector
    [SerializeField]
    private TMP_Text WarningText; //set in inspector
    [SerializeField]
    private TMP_Text PlayerText; //set in inspector
    [SerializeField]
    private int PlayerNumber; //set in inspector
    [SerializeField]
    private AudioSource audioSource; //set in inspector

    private void Awake()
    {
        Zones.Add(Base1);
        Zones.Add(Base2);
        Zones.Add(Card1);
        Zones.Add(Card2);
        Zones.Add(Card3);
        Zones.Add(Card4);
        Zones.Add(Card5);
        Zones.Add(Card6);
        Zones.Add(Card7);
        Zones.Add(Card8);
        Zones.Add(Card9);
    }

    public bool AddCard(GameObject card)
    {
        if (okay) //Se il player è ok non permette l'aggiunta della carta
        {
            WarningText.text = ("P" + (PlayerNumber + 1) + " is ok!");
            return false;
        }

        if (CardsInZone.Count < 11) //Ci possono essere massimo 11 carte nella zona delle carte
        {
            if (CardsInZone.Count < 2) //Se non sono ancora state date le due carte iniziali, aggiunge la carta
            {
                int newcardposition = CardsInZone.Count;
                Destroy(card.GetComponent<Rigidbody>());
                Destroy(card.GetComponent<Collider>());
                mouseInteractionManager.LeaveCard();
                card.GetComponent<Transform>().position = Zones[newcardposition].position;
                card.GetComponent<Transform>().rotation = Zones[newcardposition].rotation;
                CardsInZone.Add(card);
                audioSource.Play();
                TotalValue = TotalValue + CardManager.CalculateValue(card.name);
                PlayerSummary.text = "P" + (PlayerNumber + 1) + ": " + TotalValue;
                if (CardsInZone.Count == 2) //Se questa era la seconda carta data inizia la fase di decisione
                {
                    if (TotalValue < 22)
                        Decide();
                    else
                        Bust();
                }
                return true;
            }
            if (TotalValue < 22) //Se le carte iniziali sono state date, e se non ha sballato, aggiunge la carta e valuta il risultato
            {
                int newcardposition = CardsInZone.Count;
                Destroy(card.GetComponent<Rigidbody>());
                Destroy(card.GetComponent<Collider>());
                mouseInteractionManager.LeaveCard();
                card.GetComponent<Transform>().position = Zones[newcardposition].position;
                card.GetComponent<Transform>().rotation = Zones[newcardposition].rotation;
                CardsInZone.Add(card);
                audioSource.Play();
                TotalValue = TotalValue + CardManager.CalculateValue(card.name);
                PlayerSummary.text = "P" + (PlayerNumber + 1) + ": " + TotalValue;
                if (TotalValue >= 22) //Ha sballato
                {
                    Bust();
                }
                else //Decide se continuare a chiedere carta
                {
                    string animatorValue = "Surprise";
                    PlayerAnimator.SetBool(animatorValue, true);
                    StartCoroutine("ResetAnimation", animatorValue);
                    Decide();
                }
                return true;
            }
            else //Ha sballato, non si possono aggiungere altre carte
            {
                WarningText.text = ("Too many cards for player " + (PlayerNumber + 1));
                return false;
            }

        }
        else //Ha 11 carte, non si possono aggiungere altre carte
        {
            WarningText.text = ("Too many cards for player " + (PlayerNumber + 1));
            return false;
        }
    }

    private void Start()
    {
        GameObject GC = GameObject.Find("GameController");
        GameController = GC.GetComponent<GameController>();
        PlayerSummary = GameObject.Find("P" + PlayerNumber + "Summary").GetComponent<TMP_Text>();
        mouseInteractionManager = GC.GetComponent<MouseInteractionManager>();
        PlayerSummary.text = "P" + (PlayerNumber + 1) + ": " + TotalValue;
        PlayerText.text = "P" + (PlayerNumber + 1) + ": I need the first two cards";
    }

    public void Bust()
    {
        string animatorValue = "Bust";
        PlayerAnimator.SetBool(animatorValue, true);
        PlayerText.text = "P" + (PlayerNumber + 1) + ": Oh no!";
        StartCoroutine("ResetAnimation", animatorValue);
        bust = true;
    }

    private void AskCard()
    {
        string animatorValue = "AskCard";
        PlayerAnimator.SetBool(animatorValue, true);
        PlayerText.text = "P" + (PlayerNumber + 1) + ": Give me a card!";
        StartCoroutine("ResetAnimation", animatorValue);
    }

    private void Okay()
    {
        string animatorValue = "Okay";
        PlayerAnimator.SetBool(animatorValue, true);
        PlayerText.text = "P" + (PlayerNumber + 1) + ": I'm okay";
        StartCoroutine("ResetAnimation", animatorValue);
        okay = true;
    }

    private void Decide()
    {
        if (TotalValue >= 22)
        {
            Bust();
            return;
        }

        if (TotalValue < 13 && Random.Range(1, 100) < GameController.PLAYER_PERCENTAGE[PlayerNumber] && CardsInZone.Count < 11)
        {
            AskCard();
        }
        else
        {
            Okay();
        }
    }

    public void Victory()
    {
        if (PlayerAnimator.GetBool("Victory") == false)
        {
            string animatorValue = "Victory";
            PlayerAnimator.SetBool(animatorValue, true);
            PlayerText.text = "P" + (PlayerNumber + 1) + ": I win!";
        }
    }

    private IEnumerator ResetAnimation(string animatorValue)
    {
        yield return new WaitForSeconds(0.1f);
        PlayerAnimator.SetBool(animatorValue, false);
    }

    private void OnEnable()
    {
        if (PlayerAnimator.GetBool("Bust"))
            StartCoroutine("ResetAnimation", "Bust");
        else if (PlayerAnimator.GetBool("AskCard"))
            StartCoroutine("ResetAnimation", "AskCard");
        else if (PlayerAnimator.GetBool("Surprise"))
            StartCoroutine("ResetAnimation", "Surprise");
        else if (PlayerAnimator.GetBool("Okay"))
            StartCoroutine("ResetAnimation", "Okay");
        else if (PlayerAnimator.GetBool("Victory"))
            StartCoroutine("ResetAnimation", "Victory");
    }

}
